package com.epam.javatraining2016.hotelbooking;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 *
 */
public class Main {

  private static FrontOffice[] createFrontOffices(BookingQueue queue) {
    FrontOffice[] frontOffices = new FrontOffice[3];
    for (int i = 0; i < 3; i++) {
      frontOffices[i] = new FrontOffice("Front No " + i, queue, 5);
    }
    return frontOffices;
  }


  private static BackOffice[] createBackOffices(BookingQueue queue) {
    BackOffice[] retval = {new BackOffice("Back Lake", Arrays.asList(Destination.LAKE), queue),
        new BackOffice("Back Sea", Arrays.asList(Destination.SEA), queue),
        new BackOffice("Back Mountain", Arrays.asList(Destination.MOUNTAIN), queue),
        new BackOffice("Back Lake & Sea", Arrays.asList(Destination.LAKE, Destination.SEA), queue),
        new BackOffice("Back Sea & Mountain", Arrays.asList(Destination.SEA, Destination.MOUNTAIN),
            queue),
        new BackOffice("Back Mountain & Lake",
            Arrays.asList(Destination.MOUNTAIN, Destination.LAKE), queue)};
    return retval;
  }

  public static void main(String[] args) throws InterruptedException {
    Logger log = LoggerFactory.getLogger(Main.class);

    BookingQueue queue = new BookingQueue(16);

    FrontOffice[] frontOffices = createFrontOffices(queue);
    Thread[] frontOfficesThreads = new Thread[frontOffices.length];
    for (int i = 0; i < frontOffices.length; i++) {
      frontOfficesThreads[i] = new Thread(frontOffices[i], frontOffices[i].getName());
    }

    BackOffice[] backOffices = createBackOffices(queue);
    Thread[] backOfficesThreads = new Thread[backOffices.length];
    for (int i = 0; i < backOffices.length; i++) {
      backOfficesThreads[i] = new Thread(backOffices[i], backOffices[i].getName());
    }

    Runtime.getRuntime().addShutdownHook(new Thread() {
      @Override
      public void run() {
        log.info("End of work, stooping Front offices");
        for (Thread frontOffice : frontOfficesThreads) {
          frontOffice.interrupt();
        }
        for (Thread frontOffice : frontOfficesThreads) {
          while (frontOffice.isAlive()) {
            try {
              frontOffice.join();
            } catch (InterruptedException ex) {
              log.error("ShutdownHook interrupt", ex);
            }
          }
        }
        queue.close();
        for (Thread backOffice : backOfficesThreads) {
          while (backOffice.isAlive()) {
            try {
              backOffice.join();
            } catch (InterruptedException ex) {
              log.error("ShutdownHook interrupt", ex);
            }
          }
        }
      }
    });

    for (Thread frontOffice : frontOfficesThreads) {
      frontOffice.start();
    }
    for (Thread backOffice : backOfficesThreads) {
      backOffice.start();
    }

    Thread.sleep(5000);
    System.exit(0);
  }
}
