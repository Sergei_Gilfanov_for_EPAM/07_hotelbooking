package com.epam.javatraining2016.hotelbooking;

import java.util.Collection;

public class BookingRequest {
  static private int nextId = 0;
  final private Destination destination;
  final private int id;

  public BookingRequest(Destination destination) {
    this.destination = destination;
    id = BookingRequest.getId();
  }

  boolean equalsToAny(Collection<Destination> variants) {
    for (Destination variant : variants) {
      if (variant.equals(destination)) {
        return true;
      }
    }
    return false;
  }

  public String toString() {
    return String.format("BookingRequest(id=%d, destination=%s)", id, destination);
  }

  private synchronized static int getId() {
    int retval = nextId++;
    return retval;
  }
}
