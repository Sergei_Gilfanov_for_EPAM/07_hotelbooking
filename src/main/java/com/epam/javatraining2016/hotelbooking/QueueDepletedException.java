package com.epam.javatraining2016.hotelbooking;

public class QueueDepletedException extends IllegalArgumentException {
  private static final long serialVersionUID = 5307371598619812758L;
}
