package com.epam.javatraining2016.hotelbooking;

import java.util.Collection;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BackOffice implements Runnable {
  private static final Logger log = LoggerFactory.getLogger(BackOffice.class);
  private String name;
  private String logname;
  private Collection<Destination> destinations;
  private BookingQueue queue;

  public BackOffice(String name, Collection<Destination> destinations, BookingQueue queue) {
    this.name = name;
    this.logname = String.format("BackOffice(\"%s\")", name);
    this.destinations = destinations;
    this.queue = queue;

    log.info("{} created", logname);
  }

  public String getName() {
    return name;
  }

  @Override
  public void run() {
    Random random = new Random();
    while (true) {
      try {
        BookingRequest request = queue.takeIfMatch((r) -> r.equalsToAny(destinations));
        log.info("{} take {}", logname, request);
        Thread.sleep(10000 + random.nextInt(1000));
        log.info("{} done with {}", logname, request);
      } catch (QueueDepletedException e) {
        log.info("{}. No more work. Closing.", logname);
        break;
      } catch (InterruptedException e) {
        log.info("{} - Interrupted Exception. Ignoring it", logname);; // Игнорируем, т.к. нужно
                                                                       // доделать то, что лежит в
                                                                       // очереди
      }
    }
  }

}
