package com.epam.javatraining2016.hotelbooking;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class FrontOffice implements Runnable {
  private static final Logger log = LoggerFactory.getLogger(FrontOffice.class);
  private String name;
  private String logname;
  private BookingQueue queue;
  private int requestsLeft;

  FrontOffice(String name, BookingQueue queue, int requestsLimit) {
    this.name = name;
    this.logname = String.format("FrontOffice(\"%s\")", name);
    this.queue = queue;
    requestsLeft = requestsLimit;
    log.info("{} created", logname);
  }

  public String getName() {
    return name;
  }

  public void run() {
    log.info("{}.run", logname);
    Random random = new Random();
    while (true) {
      BookingRequest request = null;
      switch (random.nextInt(3)) {
        case 0:
          request = new BookingRequest(Destination.LAKE);
          break;
        case 1:
          request = new BookingRequest(Destination.MOUNTAIN);
          break;
        case 2:
          request = new BookingRequest(Destination.SEA);
          break;
      }
      log.info("{} created request {}", logname, request);
      try {
        queue.put(request);
        // Thread.sleep(random.nextInt(1000));
        if (requestsLeft-- == 0) {
          log.info("{} Requests limit reached, stopping", logname);
          break;
        } else {
          Thread.sleep(1000);
        }
      } catch (InterruptedException e) {
        log.info("{} Interrupt received, stopping", logname);
        break;
      }
    }
  }


}
