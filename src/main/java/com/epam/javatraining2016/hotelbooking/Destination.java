package com.epam.javatraining2016.hotelbooking;

public enum Destination {
  LAKE, SEA, MOUNTAIN
}
