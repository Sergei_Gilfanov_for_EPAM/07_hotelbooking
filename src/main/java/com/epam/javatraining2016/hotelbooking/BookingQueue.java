package com.epam.javatraining2016.hotelbooking;

import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.Queue;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class BookingQueue {
  private static final Logger log = LoggerFactory.getLogger(BookingQueue.class);
  private Queue<BookingRequest> requests;
  int capacity;
  boolean closed;

  public BookingQueue(int capacity) {
    requests = new ArrayDeque<BookingRequest>(capacity);
    this.capacity = capacity;
    closed = false;
  }

  /**
   * Помещает запрос в конец очереди. Если очередь уже полная, то блокируется и ждет появления
   * свободного места.
   * 
   * @param request Помещаемый в очередь запрос.
   * @throws IllegalArgumentException Выбрасывается при попытке поместить в очередь пустое значение
   * @throws IllegalStateException Выбрасывается при попытке поместить что-либо в закрытую очередь.
   * @throws InterruptedException Выбрасывается, если ожидание было прервано
   */
  public void put(BookingRequest request) throws IllegalArgumentException, InterruptedException {
    if (request == null) {
      throw new IllegalArgumentException("Can't store null values.");
    }
    synchronized (this) {
      if (closed) {
        throw new IllegalStateException("Put on closed Queue.");
      }

      while (requests.size() >= capacity) {
        log.debug("Queue is full. Waiting.");
        wait(1000);
      }
      requests.offer(request);
      log.debug("Queue accepted request {}.", request);
      notifyAll();
    }
  }

  /**
   * Извлекает из очереди запрос, соответствующий условию
   * 
   * @param predicate Условие фильтрации. Из очереди будут браться только те заявки, для которых оно
   *        будет true
   * @return
   * @throws QueueDepletedException Выбрасывается, когда заявок, соответствующих условию нет, и
   *         очередь закрыта (т.е. больше работы по данному условию не будет)
   * @throws InterruptedException
   */
  synchronized BookingRequest takeIfMatch(Predicate<BookingRequest> predicate)
      throws QueueDepletedException, InterruptedException {
    BookingRequest request;
    while (true) {
      request = findFirst(predicate);
      if (request != null) {
        break;
      }
      if (closed) {
        log.debug("Matching element not found and queue is empty");
        throw new QueueDepletedException();
      }
      log.debug("No matching elements. Waiting.");
      wait(1000);
    }
    requests.remove(request);
    notifyAll();
    log.debug("Queue returning request {}.", request);
    return request;
  }

  /**
   * Находит первый подходящий элемент, начиная с головы очереди
   * 
   * @param Проверяемое условия
   * @return Найденный элемент, если есть. null в противном случае.
   */
  private BookingRequest findFirst(Predicate<BookingRequest> p) {
    BookingRequest request;
    Iterator<BookingRequest> iterator = requests.iterator();
    while (iterator.hasNext()) {
      request = iterator.next();
      if (p.test(request)) {
        return request;
      }
    }
    return null;
  }

  /**
   * Закрытие очереди. После вызова метода очередь перестает принимать заявки (будет выбрасываться
   * IllegalStateException), и после исчерпания накопленных заявок сообщит потребителям, что работы
   * больше нет. (takeIfMatch выбросит QueueDepletedException)
   */
  void close() {
    synchronized (this) {
      closed = true;
    }
  }

}
