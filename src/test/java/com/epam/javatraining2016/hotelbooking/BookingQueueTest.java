package com.epam.javatraining2016.hotelbooking;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.javatraining2016.hotelbooking.BookingQueue;
import com.epam.javatraining2016.hotelbooking.BookingRequest;
import com.epam.javatraining2016.hotelbooking.QueueDepletedException;

import static org.mockito.Mockito.*;

import java.util.function.Predicate;

import junit.framework.TestCase;

@RunWith(MockitoJUnitRunner.class)
public class BookingQueueTest extends TestCase {
  BookingQueue queue;
  
  @Before
  public void setUp() throws Exception {
    super.setUp();
    queue = new BookingQueue(3);
  }

  @Test(expected=IllegalArgumentException.class)
  public void shouldDieWhenPutNull() throws IllegalArgumentException, InterruptedException
  {
    queue.put(null);
  }

  @Test(expected=IllegalStateException.class)
  public void shouldDieWhenPutInClosed() throws IllegalArgumentException, InterruptedException
  {
    queue.close();
    BookingRequest request = mock(BookingRequest.class);
    queue.put(request);
  }
  
  @Test
  public void takeShouldReturnFirstMatched() throws IllegalArgumentException, InterruptedException
  {
   BookingRequest[] requests = {mock(BookingRequest.class), mock(BookingRequest.class), mock(BookingRequest.class) };
   for (BookingRequest request: requests) {
     queue.put(request);
   }
   Predicate<BookingRequest> p = (request) -> (request == requests[0] || request == requests[2]);
   BookingRequest found = queue.takeIfMatch(p);
   assertThat(found, equalTo(requests[0]));
  }
  
  @Test
  public void takeShouldNotReorder() throws IllegalArgumentException, InterruptedException
  {
   BookingRequest[] requests = {mock(BookingRequest.class), mock(BookingRequest.class), mock(BookingRequest.class) };
   for (BookingRequest request: requests) {
     queue.put(request);
   }
   Predicate<BookingRequest> p = (request) -> (request == requests[0] || request == requests[2]);
   queue.takeIfMatch(p);
   BookingRequest found = queue.takeIfMatch(p);
   assertThat(found, equalTo(requests[2]));
  }
  
  @Test(expected=QueueDepletedException.class)
  public void takeShouldFinishWork() throws IllegalArgumentException, InterruptedException {
    BookingRequest[] requests = {mock(BookingRequest.class), mock(BookingRequest.class), mock(BookingRequest.class) };
    for (BookingRequest request: requests) {
      queue.put(request);
    }
    Predicate<BookingRequest> p = (request) -> (request == requests[0] || request == requests[2]);
    queue.close();
    queue.takeIfMatch(p);
    queue.takeIfMatch(p);
    queue.takeIfMatch(p); // There is no more requests, so it should die
    fail("takeIfMatch does not throw QueueDepletedException");
  }
}
